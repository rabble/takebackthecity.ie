<p align="center">
<img src="media/TBTC_logo_small.png" width="200">
</p>

This repository contains datasets used to create the [Take Back the City](https://www.takebackthecity.ie) mapping tool of Belfast and associated data-visualisations.

Each dataset is derived either from an official publicly accessible dataset, or has been generated through crowd-sourced information collection.

The original source of each dataset is attributed in the `source` field of each set. All of the data is presented in `json` format.
